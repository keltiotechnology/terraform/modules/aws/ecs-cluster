<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Usage

```hcl
locals {
  context = {
    namespace = "keltio"
    stage     = "test"
    region    = "eu-west-3"
  }

  workspace = {
    vpc_name           = "test-vpc"
    vpc_cidr_block     = "172.16.0.0/16"
    availability_zones = ["eu-west-3a", "eu-west-3b"]
  }
}


provider "aws" {
  region = "eu-west-3"
}

module "vpc" {
  source                 = "cloudposse/vpc/aws"
  version                = "0.26.1"
  namespace              = local.context["namespace"]
  name                   = local.workspace["vpc_name"]
  cidr_block             = local.workspace["vpc_cidr_block"]
  security_group_enabled = false
}

module "subnets" {
  source              = "cloudposse/dynamic-subnets/aws"
  version             = "0.39.3"
  availability_zones  = local.workspace["availability_zones"]
  vpc_id              = module.vpc.vpc_id
  igw_id              = module.vpc.igw_id
  cidr_block          = module.vpc.vpc_cidr_block
  nat_gateway_enabled = true
}

module "ecs_cluster" {
  source = "../.."

  namespace = local.context["namespace"]
  stage     = local.context["stage"]

  vpc_id  = module.vpc.vpc_id
  subnets = module.subnets.private_subnet_ids

  region             = local.context["region"]
  availability_zones = local.workspace["availability_zones"]

  ecs_security_group_ingress_rules = [
    {
      description      = "TLS from VPC"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [module.vpc.vpc_cidr_block]
      ipv6_cidr_blocks = [module.vpc.ipv6_cidr_block]
    }
  ]
  ecs_security_group_egress_rules = [
    {
      description      = "Allow all egress"
      from_port        = 0
      to_port          = 0
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
    }
  ]

  iam_task_execution_allowed_secrets_arn = []
}

```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_autoscaling_group_name"></a> [autoscaling\_group\_name](#input\_autoscaling\_group\_name) | Name of the Autoscaling group for ECS | `string` | `"asg"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | List of availability zones for ec2 instances (capacity provider) | `list(string)` | <pre>[<br>  "eu-central-1a",<br>  "eu-central-1b"<br>]</pre> | no |
| <a name="input_desired_capacity"></a> [desired\_capacity](#input\_desired\_capacity) | The number of Amazon EC2 instances that should be running in the group | `number` | `2` | no |
| <a name="input_ec2_capacity_providers"></a> [ec2\_capacity\_providers](#input\_ec2\_capacity\_providers) | Ec2 Ecs capacity providers | `list(any)` | `[]` | no |
| <a name="input_ecs_security_group_egress_rules"></a> [ecs\_security\_group\_egress\_rules](#input\_ecs\_security\_group\_egress\_rules) | Ecs security group egress rules | `list(any)` | `[]` | no |
| <a name="input_ecs_security_group_ingress_rules"></a> [ecs\_security\_group\_ingress\_rules](#input\_ecs\_security\_group\_ingress\_rules) | Ecs security group ingress rules | `list(any)` | `[]` | no |
| <a name="input_fargate_capacity_providers"></a> [fargate\_capacity\_providers](#input\_fargate\_capacity\_providers) | Fatgate Ecs capacity providers | `list(string)` | <pre>[<br>  "FARGATE_SPOT"<br>]</pre> | no |
| <a name="input_iam_task_execution_allowed_secrets_arn"></a> [iam\_task\_execution\_allowed\_secrets\_arn](#input\_iam\_task\_execution\_allowed\_secrets\_arn) | List of secrets arn that iam task execution role will have access | `list(string)` | `[]` | no |
| <a name="input_image_id"></a> [image\_id](#input\_image\_id) | AMI Image ID e.g. ami-0685fb9bda85b2783. Please refer to this docs https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html | `string` | `"ami-0685fb9bda85b2783"` | no |
| <a name="input_log_retention_in_day"></a> [log\_retention\_in\_day](#input\_log\_retention\_in\_day) | Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, and 0. If you select 0, the events in the log group are always retained and never expire. | `number` | `30` | no |
| <a name="input_max_size"></a> [max\_size](#input\_max\_size) | The maximum size of the Auto Scaling Group | `number` | `2` | no |
| <a name="input_maximum_scaling_step_size"></a> [maximum\_scaling\_step\_size](#input\_maximum\_scaling\_step\_size) | The minimum size of the Auto Scaling Group | `number` | `2` | no |
| <a name="input_min_size"></a> [min\_size](#input\_min\_size) | The minimum size of the Auto Scaling Group | `number` | `0` | no |
| <a name="input_minimum_scaling_step_size"></a> [minimum\_scaling\_step\_size](#input\_minimum\_scaling\_step\_size) | The minimum size of the Auto Scaling Group | `number` | `1` | no |
| <a name="input_name"></a> [name](#input\_name) | name | `string` | `"ecs"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | namespace | `string` | `"keltio"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | n/a | yes |
| <a name="input_ssh_key_name"></a> [ssh\_key\_name](#input\_ssh\_key\_name) | Name of the ssh key used to put on ecs instance created by ec2 capacity providers. Required only when ec2 capacity provider is given | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | stage | `string` | `"dev"` | no |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | List of subnets ids where the ecs cluster will create tasks | `list(string)` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id where to put ecs cluster | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ecs_cluster_arn"></a> [ecs\_cluster\_arn](#output\_ecs\_cluster\_arn) | n/a |
| <a name="output_ecs_cluster_name"></a> [ecs\_cluster\_name](#output\_ecs\_cluster\_name) | n/a |
| <a name="output_ecs_security_group"></a> [ecs\_security\_group](#output\_ecs\_security\_group) | n/a |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
