output "ecs_cluster_name" {
  value = local.ecs_full_cluster_name
}

output "ecs_cluster_arn" {
  value = aws_ecs_cluster.default.arn
}

output "ecs_security_group" {
  value = aws_security_group.ecs.id
}
