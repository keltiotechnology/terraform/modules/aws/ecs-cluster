#!/bin/bash

echo "Begin user data"

VALUE=$(cat <<EOF
${cloudwatch_config}
EOF
)

(echo $VALUE) | sudo sh -c 'tee -a > amazon-cloudwatch-agent.json'

sudo mkdir -p /opt/aws/amazon-cloudwatch-agent/etc/
sudo mv amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/
ls /opt/aws/amazon-cloudwatch-agent/etc/

yum install -y amazon-cloudwatch-agent
yum install -y jq

sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json

echo ECS_CLUSTER=${ecs_full_cluster_name} > /etc/ecs/ecs.config

echo "Done user data"
