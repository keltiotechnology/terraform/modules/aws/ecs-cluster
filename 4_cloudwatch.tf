/*
 * CloudWatch Logs IAM Policy
 */
resource "aws_iam_policy" "policy" {
  name        = local.ecs_cloudwatch_full_policy_name
  description = "Allow container instances to use the CloudWatch Logs APIs"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:DescribeLogStreams"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:logs:*:*:*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "cloudwatch_attach_ecsinstancerole" {
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = aws_iam_policy.policy.arn
}

/* Log groups */
// Should create ec2 logs groups when some ec2 capacity providers are provided
resource "aws_cloudwatch_log_group" "dmesg" {
  count = length(var.ec2_capacity_providers) > 0 ? 1 : 0

  name              = "${local.cloudwatch_full_prefix_name}${local.cloudwatch_log_group_dmesg}"
  retention_in_days = var.log_retention_in_day
}
resource "aws_cloudwatch_log_group" "ecs-agent" {
  count = length(var.ec2_capacity_providers) > 0 ? 1 : 0

  name              = "${local.cloudwatch_full_prefix_name}${local.cloudwatch_log_group_ecs_agent}"
  retention_in_days = var.log_retention_in_day
}
resource "aws_cloudwatch_log_group" "ecs-init" {
  count = length(var.ec2_capacity_providers) > 0 ? 1 : 0

  name              = "${local.cloudwatch_full_prefix_name}${local.cloudwatch_log_group_ecs_init}"
  retention_in_days = var.log_retention_in_day
}
resource "aws_cloudwatch_log_group" "audit" {
  count = length(var.ec2_capacity_providers) > 0 ? 1 : 0

  name              = "${local.cloudwatch_full_prefix_name}${local.cloudwatch_log_group_audit}"
  retention_in_days = var.log_retention_in_day
}
resource "aws_cloudwatch_log_group" "messages" {
  count = length(var.ec2_capacity_providers) > 0 ? 1 : 0

  name              = "${local.cloudwatch_full_prefix_name}${local.cloudwatch_log_group_messages}"
  retention_in_days = var.log_retention_in_day
}
