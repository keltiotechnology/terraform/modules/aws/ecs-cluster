resource "null_resource" "scale_down_asg" {
  count = length(var.ec2_capacity_providers)

  # https://discuss.hashicorp.com/t/how-to-rewrite-null-resource-with-local-exec-provisioner-when-destroy-to-prepare-for-deprecation-after-0-12-8/4580/2
  triggers = {
    asg_name = "autoscaling_group_name=${var.ec2_capacity_providers[count.index].autoscaling_group_name},region=${var.region}"
  }

  # Only run during destroy, do nothing for apply.
  provisioner "local-exec" {
    when        = destroy
    working_dir = path.module
    command     = <<-EOT
    python scripts/turn_off_protection.py "${self.triggers.asg_name}"
    EOT
  }
  depends_on = [aws_ecs_cluster.default]
}

// # !Image ID depends on region
resource "aws_launch_template" "ecs" {
  count = length(var.ec2_capacity_providers)

  name_prefix   = var.ec2_capacity_providers[count.index].instance_name_prefix
  image_id      = var.ec2_capacity_providers[count.index].instance_image_id
  instance_type = var.ec2_capacity_providers[count.index].instance_type
  user_data     = base64encode(data.template_file.user_data[count.index].rendered)
  key_name      = var.ec2_capacity_providers[count.index].instance_ssh_key_name

  network_interfaces {
    associate_public_ip_address = false
    security_groups             = concat([aws_security_group.ecs.id], var.ec2_capacity_providers[count.index].instance_extra_security_groups)
  }

  iam_instance_profile {
    name = aws_iam_instance_profile.ecs_instance_profile.name
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      "namespace" = var.namespace,
      "stage"     = var.stage,
      "Name"      = var.ec2_capacity_providers[count.index].instance_name_prefix
    }
  }
}

data "template_file" "user_data" {
  count = length(var.ec2_capacity_providers)

  template = file("${path.module}/templates/user_data.sh")
  vars = {
    cloudwatch_config     = local.cloudwatch_config
    ecs_full_cluster_name = local.ecs_full_cluster_name
  }
}

resource "aws_autoscaling_group" "ec2_capacity_providers" {
  count = length(var.ec2_capacity_providers)

  name             = var.ec2_capacity_providers[count.index].autoscaling_group_name
  min_size         = var.ec2_capacity_providers[count.index].autoscaling_min_size
  max_size         = var.ec2_capacity_providers[count.index].autoscaling_max_size
  desired_capacity = var.ec2_capacity_providers[count.index].autoscaling_desired_capacity

  launch_template {
    id      = aws_launch_template.ecs[count.index].id
    version = aws_launch_template.ecs[count.index].latest_version
  }

  vpc_zone_identifier   = var.subnets
  protect_from_scale_in = var.ec2_capacity_providers[count.index].autoscaling_protect_from_scale_in
  target_group_arns     = var.ec2_capacity_providers[count.index].target_group_arns

  tag {
    key                 = "AmazonECSManaged"
    value               = ""
    propagate_at_launch = true
  }

  timeouts {
    delete = "3m"
  }

  provisioner "local-exec" {
    when        = destroy
    working_dir = path.module
    command     = <<-EOT
    python scripts/turn_off_protection.py "autoscaling_group_name=${self.name},availability_zone=${tolist(self.availability_zones)[0]}"
    EOT
  }
}

resource "aws_ecs_capacity_provider" "ecs_capacity_providers" {
  count = length(var.ec2_capacity_providers)

  name = var.ec2_capacity_providers[count.index].capacity_provider_full_name

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.ec2_capacity_providers[count.index].arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size = var.ec2_capacity_providers[count.index].autoscaling_maximum_scaling_step_size
      minimum_scaling_step_size = var.ec2_capacity_providers[count.index].autoscaling_minimum_scaling_step_size
      status                    = "ENABLED"
      target_capacity           = var.ec2_capacity_providers[count.index].autoscaling_target_capacity
    }
  }
}

resource "aws_ecs_cluster" "default" {
  name = local.ecs_full_cluster_name

  # TODO : Set as variable and create multiple autoscaling group depending on variable
  capacity_providers = concat(var.fargate_capacity_providers)

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}
