/*
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Common Variables
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "region" {
  type        = string
  description = "AWS region"
}

variable "namespace" {
  type        = string
  description = "namespace"
  default     = "keltio"
}

variable "stage" {
  type        = string
  description = "stage"
  default     = "dev"
}

variable "name" {
  type        = string
  description = "name"
  default     = "ecs"
}

variable "ssh_key_name" {
  type        = string
  description = "Name of the ssh key used to put on ecs instance created by ec2 capacity providers. Required only when ec2 capacity provider is given"
  default     = ""
}

variable "vpc_id" {
  type        = string
  description = "The vpc id where to put ecs cluster"
}

variable "subnets" {
  type        = list(string)
  description = "List of subnets ids where the ecs cluster will create tasks"
}

variable "availability_zones" {
  type        = list(string)
  description = "List of availability zones for ec2 instances (capacity provider)"
  default     = ["eu-central-1a", "eu-central-1b"]
}

variable "image_id" {
  type        = string
  description = "AMI Image ID e.g. ami-0685fb9bda85b2783. Please refer to this docs https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html"
  default     = "ami-0685fb9bda85b2783"
}

variable "autoscaling_group_name" {
  type        = string
  description = "Name of the Autoscaling group for ECS"
  default     = "asg"
}

variable "min_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
  default     = 0
}

variable "max_size" {
  type        = number
  description = "The maximum size of the Auto Scaling Group"
  default     = 2
}

variable "desired_capacity" {
  type        = number
  description = "The number of Amazon EC2 instances that should be running in the group"
  default     = 2
}

variable "minimum_scaling_step_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
  default     = 1
}

variable "maximum_scaling_step_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
  default     = 2
}

variable "iam_task_execution_allowed_secrets_arn" {
  type        = list(string)
  description = "List of secrets arn that iam task execution role will have access"
  default     = []
}

variable "fargate_capacity_providers" {
  type        = list(string)
  description = "Fatgate Ecs capacity providers"
  default     = ["FARGATE_SPOT"]
}

variable "ec2_capacity_providers" {
  type        = list(any)
  description = "Ec2 Ecs capacity providers"
  default     = []
}

variable "ecs_security_group_ingress_rules" {
  type        = list(any)
  description = "Ecs security group ingress rules"
  default     = []
}

variable "ecs_security_group_egress_rules" {
  type        = list(any)
  description = "Ecs security group egress rules"
  default     = []
}

#
# Cloudwatch
##########################################################
variable "log_retention_in_day" {
  type        = number
  description = "Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, and 0. If you select 0, the events in the log group are always retained and never expire."
  default     = 30
}
