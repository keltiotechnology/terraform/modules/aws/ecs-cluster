locals {
  context = {
    namespace = "keltio"
    stage     = "test"
    region    = "eu-west-3"
  }

  workspace = {
    vpc_name           = "test-vpc"
    vpc_cidr_block     = "172.16.0.0/16"
    availability_zones = ["eu-west-3a", "eu-west-3b"]
  }
}


provider "aws" {
  region = "eu-west-3"
}

module "vpc" {
  source                 = "cloudposse/vpc/aws"
  version                = "0.26.1"
  namespace              = local.context["namespace"]
  name                   = local.workspace["vpc_name"]
  cidr_block             = local.workspace["vpc_cidr_block"]
  security_group_enabled = false
}

module "subnets" {
  source              = "cloudposse/dynamic-subnets/aws"
  version             = "0.39.3"
  availability_zones  = local.workspace["availability_zones"]
  vpc_id              = module.vpc.vpc_id
  igw_id              = module.vpc.igw_id
  cidr_block          = module.vpc.vpc_cidr_block
  nat_gateway_enabled = true
}

module "ecs_cluster" {
  source = "../.."

  namespace = local.context["namespace"]
  stage     = local.context["stage"]

  vpc_id  = module.vpc.vpc_id
  subnets = module.subnets.private_subnet_ids

  region             = local.context["region"]
  availability_zones = local.workspace["availability_zones"]

  ecs_security_group_ingress_rules = [
    {
      description      = "TLS from VPC"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [module.vpc.vpc_cidr_block]
      ipv6_cidr_blocks = [module.vpc.ipv6_cidr_block]
    }
  ]
  ecs_security_group_egress_rules = [
    {
      description      = "Allow all egress"
      from_port        = 0
      to_port          = 0
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
    }
  ]

  iam_task_execution_allowed_secrets_arn = []
}
