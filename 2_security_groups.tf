/* 
 * Provide Security Group for the ECS (EC2 instances in ECS cluster).
 * The ECS will 
 * - receive all incoming traffic from the Load Balancer.
 * - allow outcoming traffic to Load balancer and SSH clients
*/
resource "aws_security_group" "ecs" {
  name        = local.ecs_security_group_name
  description = "ECS security group : Managed by terraform"
  vpc_id      = var.vpc_id

  ingress = var.ecs_security_group_ingress_rules
  egress  = var.ecs_security_group_egress_rules

  tags = {
    Name      = local.ecs_security_group_name
    namespace = var.namespace,
    stage     = var.stage
  }
}
